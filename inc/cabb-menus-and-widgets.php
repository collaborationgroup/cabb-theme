<?php
// register a new menu
register_nav_menu('main-navigation', 'Main Navigation');

if(! function_exists('register_widgets')) {
    function register_widgets() {
        register_sidebar(
            [
                'name'          => esc_html__( 'Header' ),
                'id'            => 'header-widget',
                'description'   => esc_html__( 'Add widgets here.' ),
                'before_widget' => '<section id="%1$s" class="widget  %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<span class="widget-title">',
                'after_title'   => '</span>',
            ]
        );
        register_sidebar(
            [
                'name'          => esc_html__( 'Footer' ),
                'id'            => 'footer-widget',
                'description'   => esc_html__( 'Add widgets here.' ),
                'before_widget' => '<section id="%1$s" class="widget  %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<span class="widget-title">',
                'after_title'   => '</span>',
            ]
        );
    }
    add_action( 'widgets_init', 'register_widgets' );
}