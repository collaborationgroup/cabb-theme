<?php
error_reporting( E_ALL & ~E_DEPRECATED );
/*

This adds a custom style to prevent feeling of broken-ness in backend when
we hide E_DEPRECATED wp-admin <body> still gets a .php-error style even
when there are no error messages to show

*/
add_action('admin_head', 'ca_deprecated_php_error_fix', 100);
function ca_deprecated_php_error_fix(){
    echo '<style> .php-error #adminmenuback, .php-error #adminmenuwrap { margin-top: 0; }</style>';
}