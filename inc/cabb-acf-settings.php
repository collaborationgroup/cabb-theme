<?php
/**
 * hide ACF in admin
 */
if ( WP_ENV != 'local' && WP_ENV != 'development' ) {
	add_filter( 'acf/settings/show_admin', '__return_false' );
}

/**
 * Load json from parent theme too
 */
add_filter('acf/settings/load_json', 'parent_theme_field_groups');
function parent_theme_field_groups($paths) {
  $path = get_template_directory().'/acf-json';
  $paths[] = $path;
  return $paths;
}
