<?php
/** @var \Cabb\Theme $cabb */
global $cabb;

$global_version = $cabb->global_version;
$block_name     = 'grid';
$block_uri      = get_template_directory_uri() . '/blocks/' . $block_name;
$block_path     = get_template_directory() . '/blocks/' . $block_name;

// Grid block
$cabb_grid_id = uniqid( 'cabb_grid_id_' );
$cabb->blocks->add_block_json(
	$block_path . '/block.json',
	[
		'attributes' => [
			'grid_id' => [
				'type'    => 'string',
				'default' => $cabb_grid_id,
			],
		],
	]
);

// innerBlocks
$cabb->blocks->add_block_json( $block_path . '/inner-blocks/row/block.json', [] );
$cabb->blocks->add_block_json( $block_path . '/inner-blocks/column/block.json', [] );

add_action(
	'wp_enqueue_scripts',
	function() use ( $global_version, $block_name, $block_uri ) {
		wp_register_style( 'cabb-block-' . $block_name, $block_uri . '/style.css', [], $global_version, 'all' );
		wp_enqueue_style( 'cabb-block-' . $block_name );
	}
);

add_action(
	'enqueue_block_editor_assets',
	function() use ( $global_version, $block_name, $block_uri ) {
		wp_register_style( 'cabb-block-' . $block_name, $block_uri . '/style.css', [], $global_version, 'all' );
		wp_enqueue_style( 'cabb-block-' . $block_name );
	}
);
