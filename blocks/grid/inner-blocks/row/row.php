<?php
/** @var \Cabb\Theme $cabb */
global $cabb;
$allowed_blocks_column = array( 'acf/grid-column' );
$template_column       = array(
	array( 'acf/grid-column', array() ),
);

$row_class = [];
$row_class[] = 'row';


if ( get_field( 'xs_px' ) && get_field( 'xs_px' ) != -1 ) {
	$row_class[] = 'px-' .get_field( 'xs_px' );
}

if ( get_field( 'xs_py' ) && get_field( 'xs_py' ) ) {
	$row_class[] = 'py-' .get_field( 'xs_py' );
}

foreach ( $context['data'] as $name => $value ) {

	// Columns
	if ( strpos( $name, 'breakpoints_' ) == 0 && strpos( $name, 'cols' ) > 0 && strpos( $value, 'field' ) !== 0 ) {
		$breakpoint_size = '-' . str_replace( '_', '-', str_replace( 'breakpoints_', '', str_replace( '_cols', '', $name ) ) );

		if ( $breakpoint_size == '-xs' ) {
			$breakpoint_size = '';
		}

		if ( ! $value || $value == 0 ) {
			$value = '';
			if ($breakpoint_size == '-xs') {
				$value = 'auto';
			}
		}

		$row_class[] = 'row-cols' . $breakpoint_size . '-' . $value;
	}

	// Gutters Y
	if ( strpos( $name, 'breakpoints_' ) == 0 && strpos( $name, 'gy' ) > 0 && strpos( $value, 'field' ) !== 0 && $value != -1) {
		$breakpoint_size = '-' . str_replace( '_', '-', str_replace( 'breakpoints_', '', str_replace( '_gy', '', $name ) ) );

		if ( $breakpoint_size == '-xs' ) {
			$breakpoint_size = '';
		}

		$row_class[] = 'gy' . $breakpoint_size . '-' . $value;
	}

	// Gutters X
	if ( strpos( $name, 'breakpoints_' ) == 0 && strpos( $name, 'gx' ) > 0 && strpos( $value, 'field' ) !== 0 && $value != -1) {
		$breakpoint_size = '-' . str_replace( '_', '-', str_replace( 'breakpoints_', '', str_replace( '_gx', '', $name ) ) );

		if ( $breakpoint_size == '-xs' ) {
			$breakpoint_size = '';
		}

		$row_class[] = 'gx' . $breakpoint_size . '-' . $value;
	}

	// Vertical content position
	if ( strpos( $name, 'breakpoints_' ) == 0 && strpos( $name, 'vertical_position' ) > 0 && strpos( $value, 'field' ) !== 0 && $value != 'none' ) {
		$breakpoint_size = '-' . str_replace( '_', '-', str_replace( 'breakpoints_', '', str_replace( '_vertical_position', '', $name ) ) );

		if ( $breakpoint_size == '-xs' ) {
			$breakpoint_size = '';
		}

		$row_class[] = 'align-items' . $breakpoint_size . '-' . $value;
	}

	// Horizontal content position
	if ( strpos( $name, 'breakpoints_' ) == 0 && strpos( $name, 'horizontal_position' ) > 0 && strpos( $value, 'field' ) !== 0 && $value != 'none' ) {
		$breakpoint_size = '-' . str_replace( '_', '-', str_replace( 'breakpoints_', '', str_replace( '_horizontal_position', '', $name ) ) );

		if ( $breakpoint_size == '-xs' ) {
			$breakpoint_size = '';
		}

		$row_class[] = 'justify-content' . $breakpoint_size . '-' . $value;
	}

	// Gap
	if ( strpos( $name, 'breakpoints_' ) == 0 && strpos( $name, 'gap' ) > 0 && strpos( $value, 'field' ) !== 0 && $value != -1 && !empty($value) ) {
		$breakpoint_size = '-' . str_replace( '_', '-', str_replace( 'breakpoints_', '', str_replace( '_gap', '', $name ) ) );

		if ( $breakpoint_size == '-xs' ) {
			$breakpoint_size = '';
		}

		$row_class[] = 'gap' . $breakpoint_size . '-' . $value;
	}
}

$inner_block_html = $cabb->blocks->layout->get_head( $block, $post_id, $row_class, true, false );
$inner_block_html = str_replace( 'cabb-inner-block', '', $inner_block_html['html']);

?>
<?php if ( ! $is_preview ) { ?>
<div <?php echo $inner_block_html; ?>>
<?php } ?>
	<InnerBlocks <?php echo $inner_block_html; ?> orientation="horizontal" allowedBlocks="<?php echo esc_attr( wp_json_encode( $allowed_blocks_column ) ); ?>" template="<?php echo esc_attr( wp_json_encode( $template_column ) ); ?>" />
<?php if ( ! $is_preview ) { ?>
</div>
<?php } ?>
