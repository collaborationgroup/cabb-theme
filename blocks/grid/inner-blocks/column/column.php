<?php
/** @var \Cabb\Theme $cabb */
global $cabb;

$allowed_blocks_column_content = array(
	'acf/buttons',
);

$template_column_content = array(
	array(
		'core/paragraph',
		array(
			'content' => '',
		),
	),
);


$col_class = [];

if ( get_field( 'xs_col' ) ) {
	$col_class[] = 'col-' . get_field( 'xs_col' );
}

if ( get_field( 'sm_col' ) ) {
	$col_class[] = 'col-sm-' . get_field( 'sm_col' );
}

if ( get_field( 'md_col' ) ) {
	$col_class[] = 'col-md-' . get_field( 'md_col' );
}

if ( get_field( 'lg_col' ) ) {
	$col_class[] = 'col-lg-' . get_field( 'lg_col' );
}

if ( get_field( 'xl_col' ) ) {
	$col_class[] = 'col-xl-' . get_field( 'xl_col' );
}

if ( get_field( 'xxl_col' ) ) {
	$col_class[] = 'col-xxl-' . get_field( 'xxl_col' );
}

if ( empty( $col_class ) ) {
	$col_class[] = 'col';
}

$col_class[] = '';

if ( get_field( 'xs_flex' ) ) {
	$col_class[] = 'flex-' . get_field( 'xs_flex' );
}

if ( ! empty( get_field( 'xs_direction' ) ) && get_field( 'xs_direction' ) != 'none' ) {
	$col_class[] = 'd-flex';
	$col_class[] = 'flex-' . get_field( 'xs_direction' );
}

if ( get_field( 'xs_justify_content' ) && get_field( 'xs_justify_content' ) != 'none' ) {
	$col_class[] = 'justify-content-' . get_field( 'xs_justify_content' );
}

if ( get_field( 'xs_align_items' ) && get_field( 'xs_align_items' ) != 'none' ) {
	$col_class[] = 'align-items-' . get_field( 'xs_align_items' );
}

if ( get_field( 'xs_align_self' ) && get_field( 'xs_align_self' ) != 'none' ) {
	$col_class[] = 'align-self-' . get_field( 'xs_align_self' );
}

if ( get_field( 'xs_px' ) && get_field( 'xs_px' ) != -1 ) {
	$col_class[] = 'px-' . get_field( 'xs_px' );
}

if ( get_field( 'xs_py' ) && get_field( 'xs_py' ) != -1 ) {
	$col_class[] = 'py-' . get_field( 'xs_py' );
}



$inner_block_html = $cabb->blocks->layout->get_head( $block, $post_id, $col_class, true, false );
$inner_block_html = str_replace( 'cabb-inner-block', '', $inner_block_html['html'] );
?>
<?php if ( ! $is_preview ) { ?>
<div <?php echo $inner_block_html; ?>>
<?php } ?>
	<InnerBlocks <?php echo $inner_block_html; ?> template="<?php echo esc_attr( wp_json_encode( $template_column_content ) ); ?>" />
<?php if ( ! $is_preview ) { ?>
</div>
<?php } ?>
