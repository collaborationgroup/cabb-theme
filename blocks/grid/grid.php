<?php
/** @var \Cabb\Theme $cabb */
global $cabb;
$allowed_blocks_row = array( 'acf/grid-row' );
$template_row       = array(
	array( 'acf/grid-row' ),
);

$grid_class = [];

$container    = '';
$grid_class[] = 'container';

$fields = get_field( 'xs' );

if ( ! empty( $fields['gap'] ) && $fields['gap'] != -1  ) {
	$grid_class[] = 'gap-' . $fields['gap'];
}


$inner_block_html = $cabb->blocks->layout->get_head( $block, $post_id, $grid_class, true, false );
$inner_block_html = str_replace( 'cabb-inner-block', '', $inner_block_html['html']);
?>

<?php if ( ! $is_preview ) { ?>
<div <?php echo $inner_block_html; ?>>
<?php } ?>
	<InnerBlocks  <?php echo $inner_block_html; ?> allowedBlocks="<?php echo esc_attr( wp_json_encode( $allowed_blocks_row ) ); ?>" template="<?php echo esc_attr( wp_json_encode( $template_row ) ); ?>" />
<?php if ( ! $is_preview ) { ?>
</div>
<?php } ?>
