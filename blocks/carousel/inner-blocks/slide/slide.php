<?php
/** @var \Cabb\Theme $cabb */
global $cabb;

$template_column_content = array(
	[
		'core/group',
		[],
		[
			[
				'core/heading',
				[
					'level'       => 2,
					'placeholder' => 'Loriem ipsum',
				],
			],
			[
				'core/paragraph',
				[
					'placeholder' => 'Lorem ipsum arcu vivamus risus elementum: integer non maecenas adipiscing ultricies adipiscing sit quisque ligula sit.',
				],
			],
		],
	],
);

$block_obj = $context['carousel_id'];
global $$block_obj;

if ( ! empty( $$block_obj['items'] ) ) {
	$active = '';
} else {
	$active = ' active';
}

if ( ! isset( $$block_obj['items'] ) ) {
	$$block_obj['items'] = 1;
} else {
	$$block_obj['items']++;
}

$index           = $$block_obj['items'];
$image           = get_field( 'background_image' );
$indicator_image = get_field( 'indicator_image' );
$interval        = $context['data']['autopaly'] ?: 0;
?>
<div class="carousel-item h-100<?php echo $active; ?> slide-<?php echo $index; ?>" data-indicator-url="<?php echo wp_get_attachment_image_url( $indicator_image ); ?>" data-asset-id="<?php echo $indicator_image; ?>" data-bs-interval="<?php echo $interval * 1000; ?>">
	<?php if ( $image ) : ?>
		<?php echo wp_get_attachment_image( $image, 'large', false, [ 'class' => 'background d-block w-100 img-fluid"' ] ); ?>
	<?php endif; ?>
	<InnerBlocks <?php $cabb->blocks->layout->get_head( $block, $post_id, [ 'item-inner' ], true ); ?> template="<?php echo esc_attr( wp_json_encode( $template_column_content ) ); ?>" />
</div>
