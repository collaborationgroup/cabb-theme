<?php
/** @var \Cabb\Theme $cabb */
global $cabb;

$block_name = 'carousel';

$block_uri  = get_theme_file_uri( '/blocks/' . $block_name );
$block_path = get_template_directory() . '/blocks/' . $block_name;

add_action(
	'wp_enqueue_scripts',
	function() use ( $block_name, $block_uri ) {
		wp_register_style( 'cabb-block-' . $block_name, $block_uri . '/style.css', [], null, 'all' );
        wp_enqueue_style( 'cabb-block-' . $block_name );
		wp_register_script( 'cabb-block-' . $block_name, $block_uri . '/script.js', array( 'jquery' ), null, true );
	}
);
add_action(
	'enqueue_block_editor_assets',
	function() use ( $block_name, $block_uri ) {
		wp_register_style( 'cabb-block-' . $block_name, $block_uri . '/style.css', [], null, 'all' );
		wp_enqueue_style( 'cabb-block-' . $block_name );
        wp_register_script( 'cabb-block-' . $block_name, $block_uri . '/script.js', array( 'jquery' ), null, true );
	}
);

$carousel_id = uniqid( 'carousel_id_' );

$cabb->blocks->add_block_json(
	$block_path . '/block.json',
	[
		'attributes' => [
			'carousel_id' => [
				'type'    => 'string',
				'default' => $carousel_id,
			],
		],
	]
);

$cabb->blocks->add_block_json(
	$block_path . '/inner-blocks/slide/block.json'
);
