<?php
/** @var \Cabb\Theme $cabb */
global $cabb;

$allowed_blocks_slide = [ 'acf/slide' ];
$template_slide       = [
	[ 'acf/slide' ],
];

$carousel_id = $block['attributes']['carousel_id']['default'];
global $$carousel_id;

$$carousel_id               = [];
$$carousel_id['items']      = 0;
$$carousel_id['interval']   = get_field( 'autopaly' ) ? get_field( 'autopaly' ) : '0';
$$carousel_id['indicators'] = [];

$show_controls   = get_field( 'show_controls' ) ?? '10000';
$show_indicators = get_field( 'show_indicators' ) ?? false;
$transition      = get_field( 'transition' ) ? ' ' . get_field( 'transition' ) : '';

$block_height = get_field( 'block_height' );
$inline_style = '';
if ( ! empty( $block_height ) ) {
	$inline_style = 'height: ' . $block_height . ';';
}

$cabb->blocks->layout->get_head( $block, $post_id ); ?>
<div id="<?php echo $carousel_id; ?>" class="carousel slide w-100<?php echo $transition; ?>" data-bs-ride="carousel" style="<?php echo $inline_style; ?>">
	<?php if ( ! empty( $show_indicators ) ) : ?>
	<div class="carousel-indicators"></div>
	<?php endif; ?>
	<InnerBlocks orientation="vertical" class="carousel-inner h-100" allowedBlocks="<?php echo esc_attr( wp_json_encode( $allowed_blocks_slide ) ); ?>" template="<?php echo esc_attr( wp_json_encode( $template_slide ) ); ?>" />
	<?php if ( ! empty( $show_controls ) ) : ?>
		<button class="carousel-control-prev" type="button" data-bs-target="#<?php echo $carousel_id; ?>" data-bs-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Previous</span>
		</button>
		<button class="carousel-control-next" type="button" data-bs-target="#<?php echo $carousel_id; ?>" data-bs-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Next</span>
		</button>
	<?php endif; ?>
</div>
<?php $cabb->blocks->layout->get_foot( $block, $post_id ); ?>
<style>
</style>
<script>
jQuery(document).ready(function ($) {
	if ($('#<?php echo $carousel_id; ?> .carousel-indicators').length > 0) {
		if ($('body').hasClass('block-editor-page')) {
			function fixCarouselInPreview() {
				$('#<?php echo $carousel_id; ?> .carousel-item').each(function( index ) {
					if ( ! $(this).parent().hasClass('carousel-inner') ) {
						const carousel = document.getElementById('<?php echo $carousel_id; ?>');
						$(carousel).carousel('dispose');
						$(this).parent().wrap('<div class="'+ $(this).attr('class') +'" data-indicator-url="'+ $(this).data('indicator-url') +'" data-bs-interval="'+ $(this).data('bs-interval') +'"></div>');
						$(this).removeAttr('class');
						$(this).removeAttr('data-indicator-url');
						$(this).removeAttr('data-bs-interval');
						$(carousel).carousel();
						generateIndicators();
					}
				});
			}
			const carousel = document.getElementById('<?php echo $carousel_id; ?>');
			carousel.addEventListener('slide.bs.carousel', event => {
				fixCarouselInPreview();
			});
			fixCarouselInPreview();
		}

		function generateIndicators(){
			const carousel = document.getElementById('<?php echo $carousel_id; ?>');
			$(carousel).carousel('dispose');
			$('#<?php echo $carousel_id; ?> .carousel-indicators').empty();
			$('.carousel-item').each(function( index ) {
				var active = '';
				var current = '';
				if	( index === 0 ) {
					active = 'active';
					current = 'aria-current="true"';
				}
				$('#<?php echo $carousel_id; ?> .carousel-indicators').append('<button type="button" class="' + active + '" ' + current + ' data-bs-target="#<?php echo $carousel_id; ?>" data-bs-slide-to="' + index + '" aria-label="Slide ' + (index + 1) + '"><span><img src="'+ $(this).data('indicator-url') +'" /></span></button>');
			});
			$(carousel).carousel();
		}
		generateIndicators();
	}
});
</script>
