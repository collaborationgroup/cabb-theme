<?php
namespace Cabb;
class Theme {

	public $system;
	public $blocks;
	public $style;
	public $enqueue;
	public $global_version;

	function __construct() {

		if ( defined( 'VERSION' ) && ! empty( VERSION ) ) {
			$this->global_version = VERSION;
		} else {
			$this->global_version = wp_get_theme()->Version;
		}

		$this->system  = new System\System;
		$this->enqueue = new System\Enqueue( $this->global_version );
		$this->blocks  = new Blocks\Blocks;
		$this->style   = new Style\Style;
	}

	public function on_theme_activation() {
		/* Rewrite set up, when theme activate i mean */
		if ( isset( $_GET['activated'] ) && is_admin() ) {
			global $wp_rewrite;
			$wp_rewrite->set_permalink_structure( '/%postname%/' );
			$wp_rewrite->flush_rules();
		}
	}

	/* If theme or version is updated normally triggered after pipline or site build */
	public function on_version_uodate() {
		$cabb_build_version = get_option( 'cabb_build_version' );
		if ( $this->global_version != $cabb_build_version ) {
			add_option( 'cabb_build_version', $this->global_version );
			do_action( 'cabb_on_version_update' );
		}
	}

	public function cc_mime_types( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function admin_login_layout() {
		?>
		<style type="text/css">
		  body {
			background: <?php echo $this->style->theme_defaults['cabb_admin_login_background']; ?>;
		  }
		  body.login div#login h1 a {
			background-image: url('<?php echo get_theme_file_uri( $this->style->theme_defaults['cabb_admin_login_logo_path'] ); ?>');
			height: 65px;
			width: 220px;
			background-size: contain;
			background-repeat: no-repeat;
			padding-bottom: 0;
		  }
		  body.login div#login #nav a, body.login div#login #backtoblog a {
			color: <?php echo $this->style->theme_defaults['cabb_admin_login_link_color']; ?>
		  }
		</style>
		<?php
	}
	public function admin_login_logo_url( $url ) {
		return '/';
	}

	public function defer_scripts() {
		/**
		* Add the ability to defer or async scripts
		* @param  string $url
		* @return string
		*/
		add_filter(
			'clean_url',
			function ( $url ) {
				if ( strpos( $url, '?defer' ) ) {
					return "$url' defer='defer";
				} elseif ( strpos( $url, '?async' ) ) {
					return "$url' defer='async";
				}
				return $url;
			},
			11,
			1
		);
	}

	public function acf_should_wrap_innerblocks( $wrap, $name ) {
		if ( $name == 'acf/carousel' ) {
			return true;
		}
		return false;
	}

	public function run() {

		$this->on_theme_activation();
		$this->defer_scripts();
		$this->on_version_uodate();

		add_theme_support( 'title-tag' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'block-templates' );
		add_theme_support( 'block-template-parts' );

		// Enqueue assets
		$this->enqueue->assets( $this->style->disable_wp_customizer );

		add_action( 'acf/init', array( $this->blocks, 'init_acf_block_types' ), 10, 0 );
		add_filter( 'allowed_block_types_all', array( $this->blocks, 'allowed_block_types' ), 10, 2 );

		add_action( 'login_enqueue_scripts', [ $this, 'admin_login_layout' ] );
		add_filter( 'login_headerurl', [ $this, 'admin_login_logo_url' ] );

		add_filter( 'upload_mimes', [ $this, 'cc_mime_types' ] );

		add_filter( 'acf/blocks/wrap_frontend_innerblocks', [ $this, 'acf_should_wrap_innerblocks' ], 10, 2 );

		$this->style->generate_default_theme_settings_array();
		do_action( 'cabb_after_generate_default_theme_settings_array', $this->style->theme_settings_array );

		add_action( 'wp_enqueue_scripts', [ $this->style, 'bootstrap_css_variables' ] );
		add_action( 'enqueue_block_editor_assets', [ $this->style, 'bootstrap_css_variables' ] );

		// Set system options before this runs
		$this->system->run();

		// New json block registration
		$this->blocks->acf_block_registration();

		// Activate Customizer settings if not disabled
		if ( ! $this->style->disable_wp_customizer ) {
			$this->style->customizer == new Style\Customizer( $this->style->theme_defaults );
		}

		// Generate theme.json if not disabled
		if ( ! $this->style->disable_generate_theme_json ) {
			$this->style->generate_theme_json();
		}

	}
}
