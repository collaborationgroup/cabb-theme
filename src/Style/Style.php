<?php
namespace Cabb\Style;
class Style {
	public $customizer                  = null;
	public $disable_generate_theme_json = false;
	public $disable_wp_customizer       = false;

	public $theme_defaults = [
		'cabb_content_size'                 => '650px',
		'cabb_content_size_wide'            => '1100px',
		'cabb_body_color'                   => '#ffffff',
		'cabb_body_text_color'              => '#000000',
		'cabb_link_color'                   => '#0d6efd',
		'cabb_primary_color'                => '#0d6efd',
		'cabb_secondary_color'              => '#0d6efd',
		'cabb_third_color'                  => '#0d6efd',
		'cabb_light_color'                  => '#f8f9fa',
		'cabb_dark_color'                   => '#212529',
		'cabb_body_text_font_family'        => 'Roboto',
		'cabb_body_heading_font_family'     => 'Roboto',
		'cabb_body_text_font_size'          => '1rem',
		'cabb_body_text_line_height'        => '1.7',
		'cabb_body_text_font_weight'        => '400',
		'cabb_block_gap'                    => '3rem',

		'cabb_block_margin_y'               => '2rem',
		'cabb_block_margin_x'               => '2rem',
		'cabb_block_padding_y'              => '2rem',
		'cabb_block_padding_x'              => '2rem',

		'cabb_extra_large_font_size'        => '2.5rem',
		'cabb_large_font_size'              => '1.75rem',
		'cabb_medium_font_size'             => '1.25rem',
		'cabb_small_font_size'              => '1rem',
		'cabb_extra_small_font_size'        => '0.75rem',
		'cabb_admin_login_logo_path'        => '/assets/admin/logo.png',
		'cabb_admin_login_background'       => 'linear-gradient(to right, #0098e0 0%, #3443b1 50%, #880083 100%) !important',
		'cabb_admin_login_link_color'       => '#ffffff',

		'cabb_navbar_item_align'            => 'ms-auto',
		'cabb_navbar_padding_x'             => '0',
		'cabb_navbar_padding_y'             => '1.5rem',
		'cabb_navbar_background_color'      => 'var(--bs-primary)',
		'cabb_navbar_color'                 => 'rgba(var(--bs-primary-rgb), 0.55)',
		'cabb_navbar_hover_color'           => 'rgba(var(--bs-primary-rgb), 1)',
		'cabb_navbar_disabled_color'        => 'rgba(var(--bs-primary-rgb), 0.3)',
		'cabb_navbar_active_color'          => 'rgba(var(--bs-primary-rgb), 1)',
		'cabb_navbar_brand_padding_y'       => '0.3125rem',
		'cabb_navbar_brand_margin_end'      => '1rem',
		'cabb_navbar_brand_font_size'       => '1.25rem',
		'cabb_navbar_brand_color'           => 'rgba(var(--bs-primary-rgb), 0.9)',
		'cabb_navbar_brand_hover_color'     => 'rgba(var(--bs-primary-rgb), 0.9)',
		'cabb_navbar_nav_link_padding_x'    => '0.5rem',
		'cabb_navbar_toggler_padding_y'     => '0.25rem',
		'cabb_navbar_toggler_padding_x'     => '0.75rem',
		'cabb_navbar_toggler_font_size'     => '1.25rem',
		'cabb_navbar_toggler_icon_bg'       => 'url("data:image/svg+xml,%3csvg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 30 30\'%3e%3cpath stroke=\'rgba%280, 7, 63, 1%29\' stroke-linecap=\'round\' stroke-miterlimit=\'10\' stroke-width=\'2\' d=\'M4 7h22M4 15h22M4 23h22\'/%3e%3c/svg%3e")',
		'cabb_navbar_toggler_border_color'  => 'rgba(var(--bs-primary-rgb), 0.1)',
		'cabb_navbar_toggler_color'         => '#ffffff',
		'cabb_navbar_toggler_border_radius' => '0.375rem',
		'cabb_navbar_toggler_focus_width'   => '0.25rem',
		'cabb_navbar_toggler_transition'    => 'box-shadow 0.15s ease-in-out',

		'cabb_footer_background_color'      => 'var(--bs-primary)',
		'cabb_footer_text_color'            => 'var(--bs-white)',
	];

	public $theme_json_path = null;
	public $theme_json_hash = null;

	public $theme_settings_array = [];

	public function get_theme_json() {
		return $this->theme_settings_array;
	}

	public function is_json_generation_needed( $force_update = false ) {

		if ( $force_update || ! file_exists( $this->theme_json_path ) ) {
			return true;
		}

		// If JSON hash and last written to file matches return false
		$json = json_encode( $this->get_theme_json(), JSON_UNESCAPED_SLASHES );
		$sha1 = sha1( $json );
		if ( get_option( 'theme_json_hash' ) != $sha1 ) {
			return true;
		} else {
			return false;
		}

	}

	public function generate_theme_json() {

		if ( $this->is_json_generation_needed() ) {

			$json = json_encode( $this->get_theme_json(), JSON_UNESCAPED_SLASHES );
			$sha1 = sha1( $json );

			file_put_contents( $this->theme_json_path, $json );
			update_option( 'theme_json_hash', $sha1 );

		}

	}

	public function hex_to_rgp( $hex, $spacer = ',' ) {
		list($r, $g, $b) = sscanf( $hex, '#%02x%02x%02x' );
		return $r . $spacer . $g . $spacer . $b;
	}

	public function get_mod( $value ) {
		if ( ! $this->disable_wp_customizer ) {
			return get_theme_mod( $value, $this->theme_defaults[ $value ] );
		} else {
			return $this->theme_defaults[ $value ];
		}
	}

	public function bootstrap_css_variables() {

		$bootsrap_css_vars = ':root {
			--bs-blue: #0d6efd;
			--bs-indigo: #6610f2;
			--bs-purple: #6f42c1;
			--bs-pink: #d63384;
			--bs-red: #dc3545;
			--bs-orange: #fd7e14;
			--bs-yellow: #ffc107;
			--bs-green: #198754;
			--bs-teal: #20c997;
			--bs-cyan: #0dcaf0;
			--bs-black: #000;
			--bs-white: #fff;
			--bs-gray: #6c757d;
			--bs-gray-dark: #343a40;
			--bs-gray-100: #f8f9fa;
			--bs-gray-200: #e9ecef;
			--bs-gray-300: #dee2e6;
			--bs-gray-400: #ced4da;
			--bs-gray-500: #adb5bd;
			--bs-gray-600: #6c757d;
			--bs-gray-700: #495057;
			--bs-gray-800: #343a40;
			--bs-gray-900: #212529;
			--bs-primary: ' . $this->get_mod( 'cabb_primary_color' ) . ';
			--bs-secondary: ' . $this->get_mod( 'cabb_secondary_color' ) . ';
			--bs-success: #198754;
			--bs-info: #0dcaf0;
			--bs-warning: #ffc107;
			--bs-danger: #dc3545;
			--bs-light: ' . $this->get_mod( 'cabb_light_color' ) . ';
			--bs-dark: ' . $this->get_mod( 'cabb_dark_color' ) . ';
			--bs-primary-rgb: ' . $this->hex_to_rgp( $this->get_mod( 'cabb_primary_color' ) ) . ';
			--bs-secondary-rgb: ' . $this->hex_to_rgp( $this->get_mod( 'cabb_secondary_color' ) ) . ';
			--bs-success-rgb: 25, 135, 84;
			--bs-info-rgb: 13, 202, 240;
			--bs-warning-rgb: 255, 193, 7;
			--bs-danger-rgb: 220, 53, 69;
			--bs-light-rgb: ' . $this->hex_to_rgp( $this->get_mod( 'cabb_light_color' ) ) . ';
			--bs-dark-rgb: ' . $this->hex_to_rgp( $this->get_mod( 'cabb_dark_color' ) ) . ';
			--bs-white-rgb: 255, 255, 255;
			--bs-black-rgb: 0, 0, 0;
			--bs-body-color-rgb: ' . $this->hex_to_rgp( $this->get_mod( 'cabb_body_text_color' ) ) . ';
			--bs-body-bg-rgb: ' . $this->hex_to_rgp( $this->get_mod( 'cabb_body_color' ) ) . ';
			--bs-font-sans-serif: system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", "Noto Sans", "Liberation Sans", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
			--bs-font-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
			--bs-gradient: linear-gradient(180deg, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0));
			--bs-body-font-family: ' . $this->get_mod( 'cabb_body_text_font_family' ) . ', var(--bs-font-sans-serif);
			--bs-body-font-size: ' . $this->get_mod( 'cabb_body_text_font_size' ) . ';
			--bs-body-font-weight: ' . $this->get_mod( 'cabb_body_text_font_weight' ) . ';
			--bs-body-line-height: ' . $this->get_mod( 'cabb_body_text_line_height' ) . ';
			--bs-body-color: ' . $this->get_mod( 'cabb_body_text_color' ) . ';
			--bs-body-bg: ' . $this->get_mod( 'cabb_body_color' ) . ';
			--bs-border-width: 1px;
			--bs-border-style: solid;
			--bs-border-color: #dee2e6;
			--bs-border-color-translucent: rgba(0, 0, 0, 0.175);
			--bs-border-radius: 0.375rem;
			--bs-border-radius-sm: 0.25rem;
			--bs-border-radius-lg: 0.5rem;
			--bs-border-radius-xl: 1rem;
			--bs-border-radius-2xl: 2rem;
			--bs-border-radius-pill: 50rem;
			--bs-link-color: ' . $this->get_mod( 'cabb_link_color' ) . ';
			--bs-link-hover-color: ' . $this->get_mod( 'cabb_link_color' ) . ';
			--bs-code-color: #d63384;
			--bs-highlight-bg: #fff3cd;
		  }

		  .navbar {
			--bs-navbar-padding-x: ' . $this->get_mod( 'cabb_navbar_padding_x' ) . ';
			--bs-navbar-padding-y: ' . $this->get_mod( 'cabb_navbar_padding_y' ) . ';
			--bs-navbar-color: ' . $this->get_mod( 'cabb_navbar_color' ) . ';
			--bs-navbar-hover-color: ' . $this->get_mod( 'cabb_navbar_hover_color' ) . ';
			--bs-navbar-disabled-color: ' . $this->get_mod( 'cabb_navbar_disabled_color' ) . ';
			--bs-navbar-active-color: ' . $this->get_mod( 'cabb_navbar_active_color' ) . ';
			--bs-navbar-brand-padding-y: ' . $this->get_mod( 'cabb_navbar_brand_padding_y' ) . ';
			--bs-navbar-brand-margin-end: ' . $this->get_mod( 'cabb_navbar_brand_margin_end' ) . ';
			--bs-navbar-brand-font-size: ' . $this->get_mod( 'cabb_navbar_brand_font_size' ) . ';
			--bs-navbar-brand-color: ' . $this->get_mod( 'cabb_navbar_brand_color' ) . ';
			--bs-navbar-brand-hover-color: ' . $this->get_mod( 'cabb_navbar_brand_hover_color' ) . ';
			--bs-navbar-nav-link-padding-x: ' . $this->get_mod( 'cabb_navbar_nav_link_padding_x' ) . ';
			--bs-navbar-toggler-padding-y: ' . $this->get_mod( 'cabb_navbar_toggler_padding_y' ) . ';
			--bs-navbar-toggler-padding-x: ' . $this->get_mod( 'cabb_navbar_toggler_padding_x' ) . ';
			--bs-navbar-toggler-font-size: ' . $this->get_mod( 'cabb_navbar_toggler_font_size' ) . ';
			--bs-navbar-toggler-icon-bg: ' . $this->get_mod( 'cabb_navbar_toggler_icon_bg' ) . ';
			--bs-navbar-toggler-border-color: ' . $this->get_mod( 'cabb_navbar_toggler_border_color' ) . ';
			--bs-navbar-toggler-border-radius: ' . $this->get_mod( 'cabb_navbar_toggler_border_radius' ) . ';
			--bs-navbar-toggler-focus-width: ' . $this->get_mod( 'cabb_navbar_toggler_focus_width' ) . ';
			--bs-navbar-toggler-transition: ' . $this->get_mod( 'cabb_navbar_toggler_transition' ) . ';
		}';

		wp_add_inline_style( 'bootstrap', $bootsrap_css_vars );
	}

	function generate_default_theme_settings_array() {

		$this->theme_settings_array = [
			'$schema'  => 'https://schemas.wp.org/trunk/theme.json',
			'version'  => 2,
			'settings' => [
				'appearanceTools' => true,
				'layout'          => [
					'contentSize' => $this->get_mod( 'cabb_content_size' ),
					'wideSize'    => $this->get_mod( 'cabb_content_size_wide' ),
				],
				'custom'          => [
					'cabb-block-content-size'      => $this->get_mod( 'cabb_content_size' ),
					'cabb-block-content-wide-size' => $this->get_mod( 'cabb_content_size_wide' ),
					'cabb-block-padding-x'         => $this->get_mod( 'cabb_block_padding_x' ),
					'cabb-block-padding-y'         => $this->get_mod( 'cabb_block_padding_y' ),
					'cabb-block-margin-x'          => $this->get_mod( 'cabb_block_margin_x' ),
					'cabb-block-margin-y'          => $this->get_mod( 'cabb_block_margin_y' ),
					'cabb-navbar-background-color' => $this->get_mod( 'cabb_navbar_background_color' ),
					'cabb-footer-background-color' => $this->get_mod( 'cabb_footer_background_color' ),
					'cabb-footer-text-color'       => $this->get_mod( 'cabb_footer_text_color' ),
				],
				'color'           => [
					'custom'           => false,
					'text'             => true,
					'background'       => true,
					'defaultDuotone'   => false,
					'gradients'        => [],
					'defaultGradients' => false,
					'customGradient'   => false,
					'duotone'          => [],
					'customDuotone'    => false,
					'defaultPalette'   => false,
					'link'             => false,
					'palette'          => [
						0 => [
							'slug'  => 'primary',
							'color' => $this->get_mod( 'cabb_primary_color' ),
							'name'  => 'Primary',
						],
						1 => [
							'slug'  => 'secondary',
							'color' => $this->get_mod( 'cabb_secondary_color' ),
							'name'  => 'Secondary',
						],
						2 => [
							'slug'  => 'third',
							'color' => $this->get_mod( 'cabb_third_color' ),
							'name'  => 'Third',
						],
						3 => [
							'slug'  => 'white',
							'color' => '#ffffff',
							'name'  => 'White',
						],
						4 => [
							'slug'  => 'black',
							'color' => '#000000',
							'name'  => 'Black',
						],
					],
				],
				'border'          => [
					'color'  => false,
					'radius' => true,
					'style'  => false,
					'width'  => false,
				],
				'spacing'         => [
					'padding'           => false,
					'margin'            => false,
					'blockGap'          => false,
					'customSpacingSize' => false,
					'units'             => [
						0 => 'px',
						1 => 'em',
						2 => 'rem',
						3 => 'vh',
						4 => 'vw',
						5 => '%',
					],
				],
				'typography'      => [
					'fontFamilies'   => [
						0 => [
							'fontFamily' => '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif',
							'slug'       => 'system-fonts',
							'name'       => 'System Fonts',
						],
						1 => [
							'fontFamily' => 'roboto, sans-serif',
							'slug'       => 'roboto',
							'name'       => 'Roboto',
						],
					],
					'customFontSize' => false,
					'fontSizes'      => [
						[
							'slug' => 'x-small',
							'size' => $this->get_mod( 'cabb_extra_small_font_size' ),
							'name' => 'X-small',
						],
						[
							'slug' => 'small',
							'size' => $this->get_mod( 'cabb_small_font_size' ),
							'name' => 'Small',
						],
						[
							'slug' => 'medium',
							'size' => $this->get_mod( 'cabb_medium_font_size' ),
							'name' => 'Medium',
						],
						[
							'slug' => 'large',
							'size' => $this->get_mod( 'cabb_large_font_size' ),
							'name' => 'Large',
						],
						[
							'slug' => 'x-large',
							'size' => $this->get_mod( 'cabb_extra_large_font_size' ),
							'name' => 'X-large',
						],
					],
				],
				'blocks'          => [],
			],
			'styles'   => [
				'spacing'    => [
					'blockGap' => $this->get_mod( 'cabb_block_gap' ),
				],
				'color'      => [
					'background' => $this->get_mod( 'cabb_body_color' ),
					'text'       => $this->get_mod( 'cabb_body_text_color' ),
				],
				'typography' => [
					'fontSize'   => 'var(--wp--preset--font-size--small)',
					'fontFamily' => $this->get_mod( 'cabb_body_text_font_family' ),
					'lineHeight' => $this->get_mod( 'cabb_body_text_line_height' ),
					'fontWeight' => $this->get_mod( 'cabb_body_text_font_weight' ),
				],
				'elements'   => [
					'link' => [
						'color' => [
							'text' => $this->get_mod( 'cabb_link_color' ),
						],
					],
					'h1'   => [
						'typography' => [
							'fontSize'   => 'var(--wp--preset--font-size--x-large)',
							'fontFamily' => $this->get_mod( 'cabb_body_heading_font_family' ),
						],
					],
					'h2'   => [
						'typography' => [
							'fontSize'   => 'var(--wp--preset--font-size--large)',
							'fontFamily' => $this->get_mod( 'cabb_body_heading_font_family' ),
						],
					],
					'h3'   => [
						'typography' => [
							'fontSize'   => 'var(--wp--preset--font-size--medium)',
							'fontFamily' => $this->get_mod( 'cabb_body_heading_font_family' ),
						],
					],
					'h4'   => [
						'typography' => [
							'fontSize'   => 'var(--wp--preset--font-size--small)',
							'fontFamily' => $this->get_mod( 'cabb_body_heading_font_family' ),
						],
					],
					'h5'   => [
						'typography' => [
							'fontSize'   => 'var(--wp--preset--font-size--small)',
							'fontFamily' => $this->get_mod( 'cabb_body_heading_font_family' ),
						],
					],
					'h6'   => [
						'typography' => [
							'fontSize'   => 'var(--wp--preset--font-size--small)',
							'fontFamily' => $this->get_mod( 'cabb_body_heading_font_family' ),
						],
					],
				],
			],
		];
	}

	/**
	 * Out puts font style tag in the site header
	 *
	 * @param  string $font_path
	 * @param  bool $render
	 * @return mixed
	 */
	public function render_fonts_in_site_header( $font_path, $render = true ) {

		if ( $font_path && file_exists( $font_path ) ) {
			$output = '<style type="text/css">' . str_replace( '{{font_path}}', get_stylesheet_directory_uri(), file_get_contents( $font_path ) ) . '</style>';
			if ( $render ) {
				echo $output;
			}
			return $output;
		}
	}

	function __construct() {

		$this->theme_json_path = get_stylesheet_directory() . '/theme.json';

		add_action(
			'wp_head',
			function() {
				$this->render_fonts_in_site_header( locate_template( 'assets/fonts/fonts.css' ) );
			},
			1
		);

		add_action(
			'admin_head',
			function() {
				$this->render_fonts_in_site_header( locate_template( 'assets/fonts/fonts.css' ) );
			},
			1
		);

	}
}


