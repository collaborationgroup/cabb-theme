<?php
namespace Cabb\Style;
class Customizer {
	public $theme_defaults = [];

	public function add_color_field( $wp_customize, $name, $label, $description, $section ) {
		$wp_customize->add_setting(
			$name,
			[
				'default' => $this->theme_defaults[ $name ],
			]
		);
		$wp_customize->add_control(
			new \WP_Customize_Color_Control(
				$wp_customize,
				$name,
				[
					'label'       => $label,
					'section'     => $section,
					'description' => $description,
					'settings'    => $name,
				]
			)
		);
	}

	public function add_text_field( $wp_customize, $name, $label, $description, $section ) {

		$default = null;
		if ( isset( $this->theme_defaults[ $name ] ) ) {
			$default = $this->theme_defaults[ $name ];
		}
		$wp_customize->add_setting(
			$name,
			[
				'default' => $default,
			]
		);
		$wp_customize->add_control(
			$name,
			[
				'type'        => 'text',
				'section'     => $section,
				'label'       => $label,
				'description' => $description,
			]
		);
	}

	public function add_checkbox_field( $wp_customize, $name, $label, $description, $section ) {
		$default = null;
		if ( isset( $this->theme_defaults[ $name ] ) ) {
			$default = $this->theme_defaults[ $name ];
		}
		$wp_customize->add_setting(
			$name,
			[
				'default' => $default,
			]
		);
		$wp_customize->add_control(
			$name,
			[
				'type'        => 'checkbox',
				'section'     => $section,
				'label'       => $label,
				'description' => $description,
			]
		);
	}

	public function add_select_field( $wp_customize, $name, $label, $description, $section ) {
		if ( ! is_array( $name ) ) {
			return;
		}
		$default = null;
		if ( isset( $this->theme_defaults[ array_key_first( $name ) ] ) ) {
			$default = $this->theme_defaults[ array_key_first( $name ) ];
		}
		$wp_customize->add_setting(
			array_key_first( $name ),
			[
				'default' => $default,
			]
		);
		$wp_customize->add_control(
			array_key_first( $name ),
			[
				'label'       => $label,
				'type'        => 'select',
				'section'     => $section,
				'description' => $description,
				'choices'     => $name[ array_key_first( $name ) ],
			]
		);
	}

	public function add_field( $wp_customize, $type, $name, $label, $description, $section ) {

		if ( $type == 'color' ) {
			$this->add_color_field( $wp_customize, $name, $label, $description, $section );
		}

		if ( $type == 'text' ) {
			$this->add_text_field( $wp_customize, $name, $label, $description, $section );
		}

		if ( $type == 'checkbox' ) {
			$this->add_checkbox_field( $wp_customize, $name, $label, $description, $section );
		}

		if ( $type == 'select' ) {
			$this->add_select_field( $wp_customize, $name, $label, $description, $section );
		}

	}

	public function settings( $wp_customize ) {

		// Add logo to Site Identity
		$wp_customize->add_setting( 'cabb_theme_logo' );
		$wp_customize->add_control(
			new \WP_Customize_Image_Control(
				$wp_customize,
				'cabb_theme_logo',
				array(
					'label'       => 'Site Logo',
					'description' => 'Site logos is preferable uploaded as svg.',
					'section'     => 'title_tagline',
				)
			)
		);

		$wp_customize->add_setting( 'cabb_theme_logo_width' );
		$wp_customize->add_control(
			'cabb_theme_logo_width',
			array(
				'type'        => 'text',
				'section'     => 'title_tagline',
				'label'       => 'Site Logo width',
				'description' => 'Enter width attribute as number in px or with % as suffix.',
			)
		);

		$wp_customize->add_setting( 'cabb_theme_logo_max_width' );
		$wp_customize->add_control(
			'cabb_theme_logo_max_width',
			array(
				'type'        => 'text',
				'section'     => 'title_tagline',
				'label'       => 'Site Logo max-width',
				'description' => 'Enter max-width css value.',
			)
		);

		$wp_customize->add_setting( 'cabb_theme_logo_height' );
		$wp_customize->add_control(
			'cabb_theme_logo_height',
			array(
				'type'        => 'text',
				'section'     => 'title_tagline',
				'label'       => 'Site Logo height',
				'description' => 'Enter width attribute as number in px or with % as suffix.',
			)
		);

		$wp_customize->add_setting( 'cabb_theme_logo_max_height' );
		$wp_customize->add_control(
			'cabb_theme_logo_max_height',
			array(
				'type'        => 'text',
				'section'     => 'title_tagline',
				'label'       => 'Site Logo max-height',
				'description' => 'Enter max-width css value.',
			)
		);

		/** Navbar settings' */
		$wp_customize->add_section(
			'cabb_navbar_settings',
			array(
				'title'      => 'Navbar style',
				'priority'   => 160,
				'capability' => 'edit_theme_options',
			)
		);

		$this->add_field( $wp_customize, 'checkbox', 'cabb_navbar_disable', 'Disable navbar', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'checkbox', 'cabb_navbar_disable', 'Disable navbar', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'checkbox', 'cabb_navbar_sticky', 'Make navbar sticky', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'checkbox', 'cabb_navbar_container_outside', 'Adapt navbar width', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'checkbox', 'cabb_navbar_container_inside', 'Adapt navbar content width', '', 'cabb_navbar_settings' );
		$this->add_field(
			$wp_customize,
			'select',
			[
				'cabb_navbar_auto_hide' => [
					''               => 'Do not auto hide',
					'auto-hide'      => 'Auto hide',
					//'sub-nav'        => 'Leave sub menu',
					'hero'           => 'Auto hide and adapt to hero',
					'hero-hide-logo' => 'Auto hide, adapt to hero and hide logo',
				],
			],
			'Auto hide',
			'',
			'cabb_navbar_settings'
		);

		$this->add_field(
			$wp_customize,
			'select',
			[
				'cabb_navbar_expand' => [
					''                 => 'Never expand',
					'navbar-expand'    => 'Always expanded',
					'navbar-expand-xs' => 'XS',
					'navbar-expand-sm' => 'SM',
					'navbar-expand-md' => 'MD',
					'navbar-expand-lg' => 'LG',
					'navbar-expand-xl' => 'XL',
				],
			],
			'Expanding',
			'',
			'cabb_navbar_settings'
		);

		$this->add_field(
			$wp_customize,
			'select',
			[
				'cabb_navbar_item_align' => [
					'ms-auto'  => 'Right',
					'mx-auto'  => 'Center',
					'me-auto"' => 'Left',
				],
			],
			'Item align',
			'',
			'cabb_navbar_settings'
		);

		$this->add_field( $wp_customize, 'text', 'cabb_navbar_padding_x', 'Padding x', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_padding_y', 'Padding y', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_navbar_background_color', 'Background color', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_navbar_color', 'Text color', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_navbar_hover_color', 'Hover color', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_navbar_disabled_color', 'Disabled color', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_navbar_active_color', 'Active color', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_brand_padding_y', 'Brand padding x', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_brand_margin_end', 'Brand margin end', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_brand_font_size', 'Brand font size', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_navbar_brand_color', 'Brand color', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_navbar_brand_hover_color', 'Brand hover color', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_nav_link_padding_x', 'Link padding x', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_toggler_padding_y', 'Toggler padding y', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_toggler_padding_x', 'Toggler padding x', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_toggler_font_size', 'Toggler font size', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_toggler_icon_bg', 'Toggler icon', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_navbar_toggler_border_color', 'Toggler border color', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_toggler_border_radius', 'Toggler border radius', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_toggler_focus_width', 'Toggler focus width', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_toggler_transition', 'Toggler transition', '', 'cabb_navbar_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_navbar_extra_class', 'Extra class', '', 'cabb_navbar_settings' );

		/** Navbar settings' */
		$wp_customize->add_section(
			'cabb_footer_settings',
			array(
				'title'      => 'Footer style',
				'priority'   => 160,
				'capability' => 'edit_theme_options',
			)
		);

		$this->add_field( $wp_customize, 'color', 'cabb_footer_background_color', 'Background color', '', 'cabb_footer_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_footer_text_color', 'Text color', '', 'cabb_footer_settings' );

		/** Block style settings */
		$wp_customize->add_section(
			'cabb_block_settings',
			array(
				'title'      => 'Block style',
				'priority'   => 160,
				'capability' => 'edit_theme_options',
			)
		);
		$this->add_field( $wp_customize, 'text', 'cabb_block_gap', 'Block gap', '', 'cabb_block_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_block_padding_x', 'Block padding X', '', 'cabb_block_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_block_padding_y', 'Block padding Y', '', 'cabb_block_settings' );
		/*$this->add_field( $wp_customize, 'checkbox', 'cabb_block_decoration_enabled', 'Enable block decorations', '', 'cabb_block_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_block_decoration_top', 'Block decoration top', '', 'cabb_block_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_block_decoration_top_height', 'Block decoration top height', '', 'cabb_block_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_block_decoration_bottom', 'Block decoration bottom', '', 'cabb_block_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_block_decoration_bottom_height', 'Block decoration bottom height', '', 'cabb_block_settings' );*/

		/** Theme style settings */
		$wp_customize->add_section(
			'cabb_theme_style_settings',
			array(
				'title'      => 'Theme style',
				'priority'   => 160,
				'capability' => 'edit_theme_options',
			)
		);


		$this->add_field( $wp_customize, 'color', 'cabb_body_color', 'Body Color', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_body_text_color', 'Body Text Color', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_link_color', 'Link Color', '', 'cabb_theme_style_settings' );


		$this->add_field( $wp_customize, 'color', 'cabb_primary_color', 'Primary Color', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_secondary_color', 'Secondary Color', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_third_color', 'Third Color', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_light_color', 'Light Color', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'color', 'cabb_dark_color', 'Dark Color', '', 'cabb_theme_style_settings' );

		$this->add_field( $wp_customize, 'text', 'cabb_content_size', 'Content width', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_content_size_wide', 'Content wide width', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_body_text_font_size', 'Body font size', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_body_text_line_height', 'Body font line height', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_body_text_font_weight', 'Body font weight', '', 'cabb_theme_style_settings' );

		$this->add_field( $wp_customize, 'text', 'cabb_body_text_font_family', 'Body font family', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_body_heading_font_family', 'Heading font family', '', 'cabb_theme_style_settings' );

		$this->add_field( $wp_customize, 'text', 'cabb_extra_large_font_size', 'X-large (h1)', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_large_font_size', 'Large (h2)', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_medium_font_size', 'Medium (h3)', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_small_font_size', 'Small (h4)', '', 'cabb_theme_style_settings' );
		$this->add_field( $wp_customize, 'text', 'cabb_extra_small_font_size', 'X-small', '', 'cabb_theme_style_settings' );
	}

	public function customizer_css() {
		?>
			 <style>
				:root {
					--wp--preset--font-size--normal: <?php echo get_theme_mod( 'cabb_body_text_font_size', $this->theme_defaults['cabb_body_text_font_size'] ); ?>;
					--wp--preset--font-size--huge: <?php echo get_theme_mod( 'cabb_extra_large_font_size', $this->theme_defaults['cabb_extra_large_font_size'] ); ?>;
				}

				body {
					background-color: <?php echo get_theme_mod( 'cabb_body_color', $this->theme_defaults['cabb_body_color'] ); ?>;
					color: <?php echo get_theme_mod( 'cabb_body_text_color', $this->theme_defaults['cabb_body_text_color'] ); ?>;
					--wp--preset--color--primary: <?php echo get_theme_mod( 'cabb_primary_color', $this->theme_defaults['cabb_primary_color'] ); ?>;
					--wp--preset--color--secondary: <?php echo get_theme_mod( 'cabb_secondary_color', $this->theme_defaults['cabb_secondary_color'] ); ?>;
					--wp--preset--color--third: <?php echo get_theme_mod( 'cabb_third_color', $this->theme_defaults['cabb_third_color'] ); ?>;
					--wp--preset--font-size--small: <?php echo get_theme_mod( 'cabb_small_font_size', $this->theme_defaults['cabb_small_font_size'] ); ?>;
					--wp--preset--font-size--medium: <?php echo get_theme_mod( 'cabb_medium_font_size', $this->theme_defaults['cabb_medium_font_size'] ); ?>;
					--wp--preset--font-size--large: <?php echo get_theme_mod( 'cabb_large_font_size', $this->theme_defaults['cabb_large_font_size'] ); ?>;
					--wp--preset--font-size--x-large: <?php echo get_theme_mod( 'cabb_extra_large_font_size', $this->theme_defaults['cabb_extra_large_font_size'] ); ?>;
					--wp--style--block-gap: <?php echo get_theme_mod( 'cabb_block_gap', $this->theme_defaults['cabb_block_gap'] ); ?>;
					--wp--custom--cabb-block-padding-y: <?php echo get_theme_mod( 'cabb_block_padding_y', $this->theme_defaults['cabb_block_padding_y'] ); ?>;
					--wp--custom--cabb-block-padding-x: <?php echo get_theme_mod( 'cabb_block_padding_x', $this->theme_defaults['cabb_block_padding_x'] ); ?>;
					--wp--custom--cabb-block-margin-y: <?php echo get_theme_mod( 'cabb_block_margin_y', $this->theme_defaults['cabb_block_margin_y'] ); ?>;
					--wp--custom--cabb-block-margin-x: <?php echo get_theme_mod( 'cabb_block_margin_x', $this->theme_defaults['cabb_block_margin_x'] ); ?>;
					--wp--custom--cabb-block-contentsize: <?php echo get_theme_mod( 'cabb_content_size', $this->theme_defaults['cabb_content_size'] ); ?>;
					--wp--custom--cabb-block-widesize: <?php echo get_theme_mod( 'cabb_content_size_wide', $this->theme_defaults['cabb_content_size_wide'] ); ?>;
					--wp--custom--cabb-navbar-background-color: <?php echo get_theme_mod( 'cabb_navbar_background_color', $this->theme_defaults['cabb_navbar_background_color'] ); ?>;
					--wp--custom--cabb-footer-background-color: <?php echo get_theme_mod( 'cabb_footer_background_color', $this->theme_defaults['cabb_footer_background_color'] ); ?>;
					--wp--custom--cabb-footer-text-color: <?php echo get_theme_mod( 'cabb_footer_text_color', $this->theme_defaults['cabb_footer_text_color'] ); ?>;
				}
			 </style>
		<?php
	}

	function __construct( $theme_defaults ) {
		add_action( 'customize_register', [ $this, 'settings' ] );
		$this->theme_defaults = $theme_defaults;

		if ( is_customize_preview() ) {
			add_action( 'wp_head', [ $this, 'customizer_css' ] );
		}
	}
}
