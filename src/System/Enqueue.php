<?php
namespace Cabb\System;
class Enqueue {

	public $global_version;
	public $wp_enqueue_assets;
	public $wp_enqueue_block_editor_assets;

	public function enqueue_scripts_loop( $assets ) {
		foreach ( $assets as $type => $asset_type ) {
			foreach ( $asset_type as $file => $conf ) {

				$file = strtolower( trim( $file ) );

				/* Is external asset */
				if ( ! str_starts_with( $file, 'https://' ) && ! str_starts_with( $file, 'http://' ) && ! str_starts_with( $file, '//' ) ) {
					$file = get_theme_file_uri( $file );
				}

				switch ( $type ) {
					case 'style':
						wp_register_style( $conf['handle'], $file, $conf['deps'], $conf['ver'], $conf['media'] );
						wp_enqueue_style( $conf['handle'] );
						break;
					case 'script':
						wp_enqueue_script( $conf['handle'], $file, $conf['deps'], $conf['ver'], $conf['in_footer'] );
						break;
				}
			}
		}
	}

	/**
	 * Enqueue styles to frontend.
	 * $asset_example = [
	 *   'uri'    => '/assets/css/example.css' // Full URL or path from theme root are valid.
	 *   'handle' => 'example-handle'
	 *   'deps'   => []
	 *   'ver'    => $cabb->global_version
	 *   'media'  => string
	 *   'editor' => true // If editor is set to "true" this asset will also be enqueued in the editor.
	 *   'block_cond' => enqueue if block is used on same post_id
	 * ]
	 * @param array $asset
	 * @return void
	 */
	public function style( array $asset ) {
		$post_id = url_to_postid( home_url( $_SERVER['REQUEST_URI'] ) );
		if ( ! isset( $asset['block_cond'] ) || has_block( $asset['block_cond'], $post_id ) ) {
			if ( ! empty( $asset['uri'] ) ) {
				$this->wp_enqueue_assets['style'][ $asset['uri'] ] = $asset;
				if ( ! empty( $asset['editor'] ) && $asset['editor'] == true ) {
					$this->editor_style( $asset );
				}
			}
		}
	}

	public function editor_style( array $asset ) {
		if ( ! empty( $asset['uri'] ) ) {
			$this->wp_enqueue_block_editor_assets['style'][ $asset['uri'] ] = $asset;
		}
	}

	/**
	 * Enqueue scripts to frontend.
	 * $asset_example = [
	 *   'uri'        => '/assets/js/example.js', //Full URL or path from theme root are valid.
	 *   'handle'     => 'example-handle',
	 *   'deps'       => [],
	 *   'ver'        => $cabb->global_version,
	 *   'in_footer'  => true,
	 *   'editor'     => true, //If editor is set to "true" this asset will also be enqueued in the editor.
	 *   'block_cond' => enqueue if block is used on same post_id
	 * ]
	 * @param array $asset
	 * @return void
	 */
	public function script( array $asset ) {
		$post_id = url_to_postid( home_url( $_SERVER['REQUEST_URI'] ) );
		if ( ! isset( $asset['block_cond'] ) || has_block( $asset['block_cond'], $post_id ) ) {
			if ( ! empty( $asset['uri'] ) ) {
				$this->wp_enqueue_assets['script'][ $asset['uri'] ] = $asset;
				if ( ! empty( $asset['editor'] ) && $asset['editor'] == true ) {
					$this->editor_script( $asset );
				}
			}
		}
	}

	public function editor_script( array $asset ) {
		if ( ! empty( $asset['uri'] ) ) {
			$this->wp_enqueue_block_editor_assets['script'][ $asset['uri'] ] = $asset;
		}
	}

	public function add_customizer_assets() {
		$this->wp_enqueue_assets['style']['/assets/css/cabb-header.css']              = [
			'handle' => 'cabb-header',
			'deps'   => [ 'bootstrap' ],
			'ver'    => $this->global_version,
			'media'  => 'all',
		];
		$this->wp_enqueue_assets['style']['/assets/css/cabb-footer.css']              = [
			'handle' => 'cabb-footer',
			'deps'   => [ 'bootstrap' ],
			'ver'    => $this->global_version,
			'media'  => 'all',
		];
		$this->wp_enqueue_assets['style']['/assets/css/custom.css']                   = [
			'handle' => 'cabb-custom',
			'deps'   => [ 'bootstrap' ],
			'ver'    => $this->global_version,
			'media'  => 'all',
		];
		$this->wp_enqueue_block_editor_assets['style']['/assets/css/cabb-header.css'] = [
			'handle' => 'cabb-header',
			'deps'   => [ 'bootstrap' ],
			'ver'    => $this->global_version,
			'media'  => 'all',
		];
		$this->wp_enqueue_block_editor_assets['style']['/assets/css/cabb-footer.css'] = [
			'handle' => 'cabb-footer',
			'deps'   => [ 'bootstrap' ],
			'ver'    => $this->global_version,
			'media'  => 'all',
		];
		$this->wp_enqueue_block_editor_assets['style']['/assets/css/custom.css']      = [
			'handle' => 'cabb-custom',
			'deps'   => [ 'bootstrap' ],
			'ver'    => $this->global_version,
			'media'  => 'all',
		];
	}

	public function assets( $customizer_disabled = false ) {

		if ( ! $customizer_disabled ) {
			$this->add_customizer_assets();
		}
		add_action(
			'wp_enqueue_scripts',
			function() {
				$this->enqueue_scripts_loop( $this->wp_enqueue_assets );
			}
		);
		add_action(
			'enqueue_block_editor_assets',
			function() {
				$this->enqueue_scripts_loop( $this->wp_enqueue_block_editor_assets );
			}
		);
	}

	function __construct( $global_version ) {

		$this->global_version = $global_version;

		$this->wp_enqueue_assets = [
			'style'  => [
				'/assets/lib/bootstrap/bootstrap.css' => [
					'handle' => 'bootstrap',
					'deps'   => [],
					'ver'    => $this->global_version,
					'media'  => 'all',
				],
				'/assets/css/cabb-nav.css'            => [
					'handle' => 'cabb-nav',
					'deps'   => [ 'bootstrap' ],
					'ver'    => $this->global_version,
					'media'  => 'all',
				],
				'/assets/css/cabb-general.css'        => [
					'handle' => 'cabb-general',
					'deps'   => [ 'bootstrap' ],
					'ver'    => $this->global_version,
					'media'  => 'all',
				],
			],
			'script' => [
				'/assets/lib/bootstrap/bootstrap.bundle.min.js' => [
					'handle'    => 'bootstrap',
					'deps'      => [ 'jquery' ],
					'ver'       => $this->global_version,
					'in_footer' => true,
				],
				'/assets/js/cabb-nav.js' => [
					'handle'    => 'cabb-nav',
					'deps'      => [ 'jquery' ],
					'ver'       => $this->global_version,
					'in_footer' => true,
				],
			],
		];

		$this->wp_enqueue_block_editor_assets = [
			'style'  => [
				'/assets/lib/bootstrap/bootstrap.css'     => [
					'handle' => 'bootstrap',
					'deps'   => [],
					'ver'    => $this->global_version,
					'media'  => 'all',
				],
				'/assets/css/cabb-general.css'            => [
					'handle' => 'cabb-general',
					'deps'   => [ 'bootstrap' ],
					'ver'    => $this->global_version,
					'media'  => 'all',
				],
				'/assets/admin/css/cabb-block-editor.css' => [
					'handle' => 'cabb-block-editor',
					'deps'   => [ 'cabb-general' ],
					'ver'    => $this->global_version,
					'media'  => 'all',
				],
			],
			'script' => [
				'/assets/lib/bootstrap/bootstrap.bundle.min.js' => [
					'handle'    => 'bootstrap',
					'deps'      => [ 'jquery' ],
					'ver'       => $this->global_version,
					'in_footer' => true,
				],
				'/assets/admin/js/block-editor.js' => [
					'handle'    => 'cabb-block-editor-addon',
					'deps'      => [ 'jquery' ],
					'ver'       => $this->global_version,
					'in_footer' => false,
				],
			],
		];
	}
}
