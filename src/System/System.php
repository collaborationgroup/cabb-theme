<?php
namespace Cabb\System;

class System {

	public $disable_search = true;

	/**
	 * Disable search
	 */
	public function disable_search_parse_query( $query, $error = true ) {
		if ( is_search() && ! is_admin() ) {
			$query->is_search       = false;
			$query->query_vars['s'] = false;
			$query->query['s']      = false;

			if ( $error == true ) {
				$query->is_404 = true;
			}
		}
	}

	public function search_disabled( $disable_search = true ) {
		if ( $disable_search ) {
			add_action( 'parse_query', [ $this, 'disable_search_parse_query' ] );
			add_filter(
				'get_search_form',
				function() {
					return null;
				}
			);
		}
	}

	function run() {
		$this->search_disabled( $this->disable_search );
	}

}
