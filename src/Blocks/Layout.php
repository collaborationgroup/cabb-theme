<?php
namespace Cabb\Blocks;

class Layout {
	public $block_page_index = [];
	public $bootstrap        = false;

	public function get_head( $block, $post_id, $extra_class = [], $inner = false, $render = true ) {
		$class = [];

		$align = '';
		if ( isset( $block['align'] ) ) {
			$align = esc_attr( $block['align'] );
			if ( $this->bootstrap ) {
				switch ( $align ) {
					case 'full':
						$align = 'container-fluid';
						break;
					case 'wide':
						$align = 'container-xxl';
						break;
					default:
						$align = 'container' . ' ' . $block['align'];
				}
			} else {
				$align = 'align' . $align;
			}
		}

		$align_content = '';
		if ( isset( $block['align_content'] ) ) {
			$align_content = esc_attr( $block['align_content'] );
			if ( $this->bootstrap ) {
				switch ( $align_content ) {
					case 'top':
						$align_content = 'start';
						break;
					case 'center':
						$align_content = 'center';
						break;
					case 'bottom':
						$align_content = 'end';
						break;
					default:
						$align_content = $block['align_content'];
				}
				$align_content = 'd-flex block-align-items-' . $align_content;
			} else {
				$align_content = 'block-align-items-' . $align_content;
			}
		}

		$align_text = '';
		if ( isset( $block['align_text'] ) ) {
			$align_text = esc_attr( $block['align_text'] );
			if ( $this->bootstrap ) {
				switch ( $align_text ) {
					case 'left':
						$align_text = 'start';
						break;
					case 'center':
						$align_text = 'center';
						break;
					case 'right':
						$align_text = 'end';
						break;
					default:
						$align_text = $block['align_text'];
				}
				$align_text = 'text-' . $align_text;
			}
			$align_text = 'has-text-align-' . $align_text;
		}

		$className = '';
		if ( ! empty( $block['className'] ) ) {
			$className = $block['className'];
			if ( $this->bootstrap ) {
				switch ( $block['className'] ) {
					case 'is-style-light':
						$className = ' text-dark';
						break;
					case 'center':
						$className = ' text-dark';
						break;
				}
			}
		}

		$backgroundColor = '';
		if ( ! empty( $block['backgroundColor'] ) ) {
			$backgroundColor = 'has-' . $block['backgroundColor'] . '-background-color has-background';
		}

		$textColor = '';
		if ( ! empty( $block['textColor'] ) ) {
			$textColor = 'has-' . $block['textColor'] . '-color has-text-color';
		}

		$css = '';
		if ( ! empty( $block['style'] ) ) {
			$style_engine = wp_style_engine_get_styles( $block['style'] );
			if ( ! empty( $style_engine['css'] ) ) {
				$css = $style_engine['css'];
			}
		}

		$curr_block_index = $this->get_index( $block );
		$block_class_name = explode( '/', $block['name'] )[1];
		$class[]          = $inner ? 'cabb-inner-block' : 'cabb-block';
		$class[]          = $block_class_name;
		$class[]          = $align_content;
		$class[]          = $align;
		$class[]          = $align_text;
		$class[]          = $backgroundColor;
		$class[]          = $textColor;
		$class[]          = 'block-index-' . $curr_block_index;
		$class[]          = $className;
		$class            = array_merge( $class, $extra_class );
		$html_class       = str_replace( '  ', ' ', trim( implode( ' ', $class ) ) );
		$html_style       = $css ? ' style="' . $css . '"' : '';

		if ( $inner ) {
			$html = 'id="' . $block['id'] . '" class="' . $html_class . '"' . $html_style;
		} else {
			$html = '<div id="' . $block['id'] . '" class="' . $html_class . '"' . $html_style . '>';
		}

		$return = [
			'block_id'    => $block['id'],
			'class_names' => $class,
			'class'       => $html_class,
			'html'        => $html,
		];

		if ( $render ) {
			echo $html;
		}
		return $return;
	}

	public function get_foot( $block, $post_id, $render = true ) {
		$html = '</div>';

		if ( $render ) {
			echo $html;
		}
		return $html;
	}

	public function get_index( $block ) {
		global $post;

		if ( ! $post ) {
			return;
		}

		if ( array_key_exists( $block['id'], $this->block_page_index ) ) {
			return $this->block_page_index[ $block['id'] ];
		}

		$curr_block_index = '';
		$block_index      = 0;
		$post_blocks      = parse_blocks( $post->post_content );
		foreach ( $post_blocks as $key => $value ) {
			if ( isset( $value['attrs'] ) && isset( $value['attrs']['id'] ) ) {
				$block_index++;
				if ( $value['attrs']['id'] == $block['id'] ) {
					$curr_block_index                       = $block_index;
					$this->block_page_index[ $block['id'] ] = $block_index;
					break;
				}
			}
		}
		return $curr_block_index;
	}

	function get_heading( $block, $string = '', $heading_level = 1, $class = null ) {
		if ( $this->get_index( $block ) == 1 ) {
			$heading_level = 1;
		} else {
			if ( (int) $heading_level = 1 ) {
				$heading_level = 2;
			}
		}

		if ( $class ) {
			$class = ' class="' . $class . '"';
		}

		return '<h' . $heading_level . $class . '>' . $string . '</h' . $heading_level . '>';
	}
}
