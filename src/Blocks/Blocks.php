<?php
namespace Cabb\Blocks;
class Blocks {
	public $enabled_blocks          = [];
	public $acf_blocks_to_register  = [];
	public $disabled_blocks_by_name = [];
	public $allowed_default_blocks  = [
		'core/group',
		'core/media-text',
		'core/more',
		'core/table',
		'core/list',
		'core/gallery',
		'core/cover',
		'core/heading',
		'core/video',
		'core/image',
		'core/column',
		'core/columns',
		'core/paragraph',
		'core/buttons',
		'core/button',
		'core/separator',
		'core/spacer',
		'core/navigation',
		'core/navigation-link',
		'core/navigation-submenu',
		'core/site-logo',
		'core/site-title',
		'core/post-excerpt',
		'core/post-featured-image',
		'core/query',
		'core/shortcode',
		'core/social-links',
		'core/social-link',
	];
	public $block_style_to_header   = [];
	public $layout;

	function __construct() {
		$this->layout = new Layout;
	}

	public function acf_block_registration() {
		add_action( 'init', [ $this, 'register_acf_blocks' ], 5 );
	}

	public function register_acf_blocks() {
		foreach ( $this->acf_blocks_to_register as $value ) {
			if ( ! empty( $value['path'] ) ) {
				$path = $value['path'] ?? '';
				$args = $value['args'] ?? [];
				register_block_type( $path, $args );
			}
		}
	}
	public function add_block_json( string $path, array $args = [] ) {
		$arr                            = [
			'path' => $path,
			'args' => $args,
		];
		$this->acf_blocks_to_register[] = $arr;
	}

	/**
	 * Add block to the enable block list.
	 * Deprecated! Use add_block_json.
	 */
	public function add_block( array $block ) {
		$this->enabled_blocks[] = $block;
	}

	/**
	 * Disable default CABB block by an array of block name.
	 * Deprecated!
	 */
	public function set_disable_cabb_blocks( array $block = [] ) {
		$this->allowed_default_blocks[] = $block;
	}

	/**
	 * Add blocks to the allowed allowed_default_blocks array
	 */
	public function add_allowed_blocks( array $block ) {
		$this->allowed_default_blocks = array_merge( $this->allowed_default_blocks, $block );
	}

	/**
	 * Enable default Gutenberg blocks by an array of block names (prefix/name).
	 * Depticated!
	 */
	public function set_allowed_gutenberg_blocks( array $block = [] ) {
		$this->disabled_blocks_by_name[] = $block;
	}

	/**
	 * Get an array of enabled blocks.
	 */
	public function get_enabled_blocks() {
		return $this->enabled_blocks;
	}

	/**
	 * Register ACF blocks, enqueue style and scripts.
	 */
	public function init_acf_block_types() {

		// Get cabb blocks
		$block_path      = get_template_directory() . '/blocks/*/init.php';
		$block_init_list = str_replace( get_template_directory(), '', glob( $block_path ) );

		// Get blocks from child theme
		if ( is_child_theme() ) {
			$child_theme_block_path      = get_stylesheet_directory() . '/blocks/*/init.php';
			$child_theme_block_init_list = str_replace( get_stylesheet_directory(), '', glob( $child_theme_block_path ) );
			$block_init_list             = array_unique( array_merge( $child_theme_block_init_list, $block_init_list ) );
		}

		// Include available init.php:s
		foreach ( $block_init_list as $file ) {
			include get_theme_file_path( $file );
		}

		// Register blocks
		if ( function_exists( 'acf_register_block_type' ) ) {
			foreach ( $this->enabled_blocks as $block ) {
				if ( ! in_array( $block['name'], $this->disabled_blocks_by_name ) ) {
					acf_register_block_type( $block );
					if ( ! empty( $block['cabb'] ) && ! empty( $block['cabb']['header_enqueue_styles'] ) ) {
						$this->block_style_to_header[ $block['name'] ] = $block['cabb']['header_enqueue_styles'];
					}
				}
			}
			add_action( 'wp_enqueue_scripts', [ $this, 'add_block_style_to_header' ] );
			add_action( 'admin_enqueue_scripts', [ $this, 'add_block_style_to_header' ] );
		}
	}

	/**
	 * Register the styles (CSS) for the blocks outside
	 * acf_register_block_type() as loading styles
	 * using acf_register_block_type() will load the
	 * styles in the footer and not in <head> causing
	 * CLS issues
	 */
	public function add_block_style_to_header() {
		foreach ( $this->block_style_to_header as $name => $style ) {
			if ( has_block( 'acf/' . $name ) ) {
				foreach ( $style  as $style_args ) {
					if ( empty( $style_args['handle'] ) ) {
						$style_args['handle'] = '';
					}
					if ( empty( $style_args['src'] ) ) {
						$style_args['src'] = '';
					}
					if ( empty( $style_args['deps'] ) ) {
						$style_args['deps'] = [];
					}
					if ( empty( $style_args['ver'] ) ) {
						$style_args['ver'] = '';
					}
					if ( empty( $style_args['media'] ) ) {
						$style_args['media'] = '';
					}
					wp_enqueue_style( $style_args['handle'], $style_args['src'], $style_args['deps'], $style_args['ver'], $style_args['media'] );
				}
			}
		}
	}

	public function allowed_block_types( $allowed_block_types, $block_editor_context ) {

		if ( $this->allowed_default_blocks === null ) {
			$this->allowed_default_blocks = $allowed_block_types;
		}
		$this->allowed_default_blocks = array_merge(
			$this->allowed_default_blocks,
			array_map(
				function( $value ) {
					return 'acf/' . $value;
				},
				array_column( $this->enabled_blocks, 'name' )
			)
		);

		$allowed_blocks = array_merge( $this->allowed_default_blocks, $this->disabled_blocks_by_name );
		$block_types    = \WP_Block_Type_Registry::get_instance()->get_all_registered();
		foreach ( $block_types as $block ) {

			if ( ! in_array( $block->name, $allowed_blocks ) && ! str_contains( $block->name, 'core/' ) ) {
				$allowed_blocks[] = $block->name;
			}
		}

		return $allowed_blocks;
	}

}
