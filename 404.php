<?php
get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<div class="inner">
				<h1>404</h1>
				<p><?php _e( 'Oops! Page not found.' ); ?></p>
				<div class="wp-block-button has-custom-font-size is-style-outline has-small-font-size"><a href="/" class="wp-block-button__link has-primary-color has-text-color"><?php _e( 'Go to front page' ); ?></a></div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
