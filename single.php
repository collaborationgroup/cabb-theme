<?php get_header(); ?>
	<main class="window-width" role="main">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) :
				the_post();
				?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php get_template_part( 'template-parts/content-modules' ); ?>

			</article>
					<?php endwhile; ?>
		<?php else : ?>

			<article>

				<h2><?php _e( 'Sorry, nothing to display.' ); ?></h2>

			</article>

		<?php endif; ?>
	</main>
<?php get_footer(); ?>
