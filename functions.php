<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

require get_theme_file_path( 'inc/cabb-php8-fix.php' );
require get_theme_file_path( 'inc/cabb-menus-and-widgets.php' );
require get_theme_file_path( 'inc/cabb-bootstrap-nav-walker.php' );
require get_theme_file_path( 'inc/cabb-acf-settings.php' );

if ( ! function_exists( 'cabb_theme_setup' ) ) {
	function cabb_theme_setup() {

		if ( ! isset( $cabb ) ) {
			global $cabb;
			$cabb = new \Cabb\Theme;

			// Run cabb theme
			$cabb->run();
		}

	}
}
add_action( 'after_setup_theme', 'cabb_theme_setup' );
