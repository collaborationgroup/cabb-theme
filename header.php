<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php
$navbar_class   = [];
$navbar_class[] = 'collapsed';
$nav_expand     = '';

if ( get_theme_mod( 'cabb_navbar_expand' ) ) {
	$nav_expand     = get_theme_mod( 'cabb_navbar_expand' );
	$navbar_class[] = $nav_expand;
}

$cabb_navbar_disable                 = false;
$cabb_navbar_sticky                  = '';
$cabb_navbar_disable_auto_hide       = true;
$cabb_navbar_adapt_to_hero           = false;
$cabb_navbar_adapt_to_hero_hide_logo = false;

if ( get_theme_mod( 'cabb_navbar_disable' ) ) {
	$navbar_class[] = get_theme_mod( 'cabb_navbar_disable' );
}

if ( get_theme_mod( 'cabb_navbar_sticky' ) ) {
	$cabb_navbar_sticky = 'sticky-top';

	$cabb_navbar_auto_hide = get_theme_mod( 'cabb_navbar_auto_hide' );
	if ( $cabb_navbar_auto_hide == 'auto-hide' ) {
		$cabb_navbar_disable_auto_hide = false;
	}

	if ( $cabb_navbar_auto_hide == 'hero' ) {
		$cabb_navbar_disable_auto_hide = false;
		$cabb_navbar_adapt_to_hero     = true;
		$navbar_class[]                = 'cabb-adapt';
		$cabb_navbar_sticky            = 'fixed-top';
	}

	if ( $cabb_navbar_auto_hide == 'hero-hide-logo' ) {
		$cabb_navbar_disable_auto_hide       = false;
		$cabb_navbar_adapt_to_hero           = true;
		$cabb_navbar_adapt_to_hero_hide_logo = true;
		$navbar_class[]                      = 'cabb-adapt';
		$cabb_navbar_sticky                  = 'fixed-top';
	}

	$navbar_class[] = $cabb_navbar_sticky;
}

if ( get_theme_mod( 'cabb_navbar_extra_class' ) ) {
	$navbar_class[] = get_theme_mod( 'cabb_navbar_extr_class' );
}

$nav_auto_hide = '';
if ( get_theme_mod( 'cabb_navbar_auto_hide' ) ) {
	$navbar_class[] = get_theme_mod( 'cabb_navbar_auto_hide' );
}
?>
<?php if ( ! get_theme_mod( 'cabb_navbar_disable' ) ) : ?>
<?php if ( get_theme_mod( 'cabb_navbar_container_outside' ) ) : ?>
<div class="container">
<?php endif; ?>
<nav id="navbar" class="navbar <?php echo implode( ' ', $navbar_class ); ?>" data-disable-auto-hide="<?php echo $cabb_navbar_disable_auto_hide ? 'true' : 'false'; ?>" data-show-on-upscroll="true" data-show-on-bottom="false" data-hide-offset="auto" data-animation-duration="200" data-adapt-to-hero="<?php echo $cabb_navbar_adapt_to_hero ? 'true' : 'false'; ?>" data-adapt-to-hero-hide-logo="<?php echo $cabb_navbar_adapt_to_hero_hide_logo ? 'true' : 'false'; ?>" >
	<?php if ( get_theme_mod( 'cabb_navbar_container_inside' ) ) : ?>
	<div class="container">
	<?php else : ?>
	<div class="container-fluid">
	<?php endif; ?>
		<a class="navbar-brand" href="/">
			<?php if ( get_theme_mod( 'cabb_theme_logo' ) ) : ?>
				<?php
				$theme_logo_width = '';
				if ( get_theme_mod( 'cabb_theme_logo_width' ) ) {
					$theme_logo_width = get_theme_mod( 'cabb_theme_logo_width' );
				}

				$theme_logo_height = '';
				if ( get_theme_mod( 'cabb_theme_logo_height' ) ) {
					$theme_logo_height = get_theme_mod( 'cabb_theme_logo_height' );
				}
				?>
				<img class="align-middle logo" width="<?php echo $theme_logo_width; ?>" height="<?php echo $theme_logo_height; ?>" src="<?php echo get_theme_mod( 'cabb_theme_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" >
			<?php else : ?>
				<?php bloginfo( 'name' ); ?>
			<?php endif; ?>
		</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="main-menu">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'main-navigation',
					'container'      => false,
					'menu_class'     => get_theme_mod( 'cabb_navbar_item_align', 'ms-auto' ),
					'fallback_cb'    => '__return_false',
					'items_wrap'     => '<ul id="%1$s" class="navbar-nav %2$s">%3$s</ul>',
					'depth'          => 2,
					'walker'         => new bootstrap_5_wp_nav_menu_walker(),
				)
			);
			?>
		</div>
	</div>
</nav>
	<?php if ( get_theme_mod( 'cabb_navbar_container_outside' ) ) : ?>
</div>
<?php endif; ?>
<?php endif; // cabb_navbar_disable ?>


<!--header>
<?php
block_template_part( 'header' );
if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'header-widget' ) ) :
	endif;
?>
</header-->

